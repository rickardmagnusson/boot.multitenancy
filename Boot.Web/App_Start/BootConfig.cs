﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Boot.Multitenancy;
using Boot.Multitenancy.Factory;

using System.Data.SqlServerCe;

namespace Boot.Web
{
    public static class BootConfig
    {
        public static void Init()
        {
            foreach (var tenant in Host.TenantCollection)
            {
                try {
                    if (!tenant.Exist())
                    {
                        var engine = new SqlCeEngine(tenant.Configuration.Connectionstring);
                        engine.CreateDatabase();
                    }
                }
                catch (Exception ex) { throw ex; }
            }

            Host.Init();
        }
    }
}