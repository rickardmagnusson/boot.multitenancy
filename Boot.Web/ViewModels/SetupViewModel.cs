﻿using Boot.Multitenancy.Intrastructure.Domain;
using Boot.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Boot.Web.ViewModels
{ 
    public class SetupViewModel
    {
        public List<Type> GetTypes{ get{ return GetInstalledTypes(); } }
        public List<string> EntityNames { get; set; }
        public string SiteName { get; set; }
        public string FooterText { get; set; }
        public string Connectionstring { get; set; }
        public List<DbType> DbTypes { get; set; }
        public DbType SelectedType { get; set; }


        public SetupViewModel()
        {
            EntityNames = new List<string>();
            DbTypes = new List<DbType> { DbType.MySql5, DbType.SqlCe, DbType.SqlServer2008 };
        }

        /// <summary>
        /// Grab all installed types by Fluent
        /// </summary>
        /// <returns>A list of name of the types.</returns>
        public List<Type> GetInstalledTypes()
        {
            var Types = new List<Type>();

            foreach (var assemblyName in Assembly.GetExecutingAssembly().GetReferencedAssemblies())
            {
                var assembly = Assembly.Load(assemblyName);
                foreach (var type in assembly.GetTypes())
                {
                    if (type.IsClass && (type.GetInterface("IEntity") != null) && (!type.IsAbstract))
                    {
                        Types.Add(type);
                    }
                }
            }
            return Types;
        }
    }
}