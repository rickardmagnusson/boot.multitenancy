﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boot.Multitenancy.Intrastructure.Domain
{

    /// <summary>
    /// Placeholder of a hierarchial item.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NodeBase<T>
    {
        public NodeBase(T data)
        {
            Data = data;
            Nodes = new List<NodeBase<T>>();
        }
        public T Data { get; private set; }
        public IEnumerable<NodeBase<T>> Nodes { get; private set; }
    }
}
