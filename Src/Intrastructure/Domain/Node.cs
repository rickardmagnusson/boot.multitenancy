﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boot.Multitenancy.Intrastructure.Domain
{

    /// <summary>
    /// The base class for hierarchy item.
    /// </summary>
    public abstract class Node
    {
        public virtual Int32 Id { get; set; }
        public virtual Int32 ParentId { get; set; }
    }
}
