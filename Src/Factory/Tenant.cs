﻿using System;
using System.Linq;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Boot.Multitenancy.Intrastructure;
using Boot.Multitenancy.Extensions;
using Boot.Multitenancy.Intrastructure.Domain;
using Boot.Multitenancy.Filters;
using System.Diagnostics;

namespace Boot.Multitenancy.Factory
{
    /// <summary>
    /// Tenant
    /// </summary>
    public class Tenant : ITenant
    {

        /// <summary>
        /// The Configuration for this Tenant
        /// </summary>
        public ITenantConfiguration Configuration { get; set; }



        //Ctor can only be created internal.
        internal Tenant() { }



        /// <summary>
        /// Only for Sql server Compact
        /// Check if the .sfd file exist.
        /// If AutoPersist is set to false, this function return false.
        /// </summary>
        /// <returns>If database exist.</returns>
        public bool Exist()
        {
            if (Configuration.AutoPersist && Configuration.DbType == DbType.SqlCe)
            {
                var path = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
                var fullPath = string.Join("\\", new[] { path, Configuration.Name.ToLower() + ".sdf" });
                if (System.IO.File.Exists(fullPath))
                    return true;
            }
            return false;
        }



        /// <summary>
        /// Init a new Tenant with ITenantConfiguration.
        /// </summary>
        /// <param name="configuration">ITenantConfiguration configuration</param>
        public Tenant(ITenantConfiguration configuration)
        {
            Configuration = configuration;
        }

        


        /// <summary>
        /// Creates the ISessionFactory configuration.
        /// </summary>
        /// <returns>ISessionFactory of current configuation.</returns>
        public ISessionFactory CreateConfig()
        {
            return FluentConfiguration
                .BuildSessionFactory();
        }



        /// <summary>
        /// The current FluentConfiguration
        /// </summary>
        public FluentConfiguration FluentConfiguration
        {
            get
            {
                return Fluently
                  .Configure()
                     .Database(DatabaseConfiguration)
                        .Mappings(MapAssemblies)
                           .ExposeConfiguration(BuildSchema)
                               .ExposeConfiguration(c => c.SetInterceptor(new SqlPrintInterceptor()));
            }
        }



        /// <summary>
        /// Searches all assemblies for IEntity's
        /// </summary>
        /// <param name="fmc">MappingConfiguration</param>
        internal void MapAssemblies(MappingConfiguration fmc)
        {
            (from a in AppDomain.CurrentDomain.GetAssemblies()
             select a
                 into assemblies
                     select assemblies)
                        .ToList()
                            .ForEach(a => {
                                fmc.AutoMappings.Add(AutoMap.Assembly(a)
                                   .OverrideAll(p => {
                                        p.SkipProperty(typeof(NoProperty));})
                                            .Conventions.Add<StringColumnLengthConvention>()
                                                .Where(IsEntity));
                            });
        }



        /// <summary>
        /// Creates the configuration for FluentnHibernate
        /// </summary>
        /// <returns>IPersistenceConfigurer config</returns>
        internal IPersistenceConfigurer DatabaseConfiguration()
        {
            switch (Configuration.DbType)
            {
                case DbType.MySql5:
                    return MySQLConfiguration.Standard
                            .UseOuterJoin()
                            .ConnectionString(Configuration.Connectionstring)
                            .ShowSql();
                case DbType.SqlCe:
                    return MsSqlCeConfiguration.MsSqlCe40
                            .UseOuterJoin()
                            .ConnectionString(Configuration.Connectionstring)
                            .ShowSql();
                case DbType.SqlServer2008:
                    return MsSqlConfiguration.MsSql2008
                            .UseOuterJoin()
                            .ConnectionString(Configuration.Connectionstring)
                            .ShowSql();
                default:
                    return null;
            }
        }



        /// <summary>
        /// Builds the configuration for FluentnHibernate/nHibernate
        /// </summary>
        /// <param name="config"></param>
        internal void BuildSchema(NHibernate.Cfg.Configuration config)
        {
            SchemaMetadataUpdater.QuoteTableAndColumns(config);
            string path = string.Empty.GetConfigFile();

            new SchemaExport(config).SetDelimiter(";").SetOutputFile(path).Create(true, false);
            new SchemaUpdate(config).Execute(false, true);
        }



        /// <summary>
        /// The Entity to look for
        /// </summary>
        /// <param name="t">The type to compare</param>
        /// <returns>If type is Entity</returns>
        private static bool IsEntity(Type t)
        {
            return typeof(IEntity).IsAssignableFrom(t);
        }



        public class SqlPrintInterceptor : EmptyInterceptor
        {
            public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
            {
#if DEBUG
                string path = "sqloutput.txt".GetFile();
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(path,true))
            {  
                file.Write(Environment.NewLine + sql.ToString());
            }
            Trace.WriteLine(sql.ToString());
#endif
                return sql;
            }
        }
    }
}
