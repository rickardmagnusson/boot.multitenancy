﻿using System.Collections.Generic;
using Boot.Multitenancy.Factory;
using Boot.Multitenancy.SessionManager;
using Boot.Multitenancy.Configuration;
using System.Collections;
using System.Linq;
using Boot.Multitenancy.Extensions;
using Con = Boot.Multitenancy.Configuration.ConnectionstringConfiguration;
using Conf = Boot.Multitenancy;
using System;
using System.Text;
using NHibernate;

namespace Boot.Multitenancy
{

    /// <summary>
    /// The default Host factory. Creates Tenants.
    /// </summary>
    public class Host : IHost
    {

        private static readonly object Lock = new object();
        public static Host FactoryHost { get; private set; }
        internal TenantCollection Tenants { get; set; }
        internal static bool IsOpen { get; set; }


        /// <summary>
        /// Static Ctor
        /// </summary>
        static Host()
        {
            lock (Lock)
            {
                IsOpen = false;
                FactoryHost = new Host();
            }
        }

        /// <summary>
        /// Singleton implementation
        /// </summary>
        private Host()
        {
            Tenants = new TenantCollection();
        }


        /// <summary>
        /// Init configuration from web.config
        /// </summary>
        public static void Init()
        {
            bool noTenants = (FactoryHost.Tenants != null);
            bool useConfig = CreateEnvironment();

            if (noTenants && useConfig) //See if tenants already created.
                CreateTenants();

            Init(FactoryHost.Tenants);
        }


        /// <summary>
        /// Reads out configuration before creating scripts for database.
        /// Usually used to create database before init configuration.
        /// When ready, Call Init();
        /// </summary>
        /// <returns>A list of ConnectionElement</returns>
        public static List<ConnectionElement> PreInit()
        {
            var connectionElements = new List<ConnectionElement>();
            (from d in Databases select d)
                    .ToList()
                        .ForEach(p => {
                            connectionElements.Add(
                                new ConnectionElement
                                {
                                    Name = p.Name,
                                    Connectionstring = Con.CreateConnectionstring(p.DbType, p.Name)
                                });
                        });

            return connectionElements;
        }


        /// <summary>
        /// Offers a way to make updates to collection by restart/reset the Host.
        /// </summary>
        public static void Restart()
        {
            FactoryHost.Tenants.Clear();
        }


        /// <summary>
        /// Helper 
        /// </summary>
        /// <returns></returns>
        public static ISession Open()
        {
            if (IsOpen)
                return SessionFactoryHostContainer.CurrentFactory.OpenSession();
            else
                throw new Exception("Host.Init() is not called yet!!");
        }


        /// <summary>
        /// Open with specifc class.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ISession OpenWith<T>()
        {
            if (IsOpen)
                return SessionHostFactory.With<T>().OpenSession();
            else
                throw new Exception("Host.Init() is not called yet!!");
        }


        /// <summary>
        /// Intit configuration with a List of Tenants
        /// </summary>
        /// <param name="tenants">TenantCollection to add</param>
        public static void Init(TenantCollection tenants)
        {
            FactoryHost.Tenants = tenants;
            FactoryHost.Tenants.Validate();

            (from tenant in FactoryHost.Tenants 
              select tenant)
                .ToList()
                    .ForEach(t =>  {
                        var tenant = (Tenant)t.Value;
                        tenant.Configuration.SessionFactory = tenant.CreateConfig();

                        if (tenant.Configuration.HostValues == null)
                            tenant.Configuration.HostValues = new List<string>();

                        if (tenant.Configuration.Properties == null)
                            tenant.Configuration.Properties = new Dictionary<string, object>();
                            
                        SessionFactoryHostContainer.Current.Add(tenant);
                    });
            IsOpen = true;
        }


        /// <summary>
        /// Builds the configuration from web.config
        /// </summary>
        private static void CreateTenants()
        {
            foreach (var element in Databases) 
            {
                var conf = new TenantConfiguration
                {
                    Key = element.Name,
                    Name = element.Name,
                    DbType = element.DbType,
                    HostValues = element.DomainList,
                    Properties = element.PropertyList,
                    AutoPersist = element.AutoPersist,
                    Connectionstring = element.Connectionstring
                };

                var tenant = new Tenant() { Configuration = conf };
                FactoryHost.Tenants.Add(element.Name, tenant);
            }
        }


        /// <summary>
        /// Reads out configuration before creating FluentConfiguration.
        /// Does not affect the created collection.
        /// </summary>
        /// <returns>A list of ConnectionElement</returns>
        public static TenantCollection ConfigCollection
        {
            get{
                var tenants = new TenantCollection();
                foreach (var element in Databases)
                {
                    var conf = new TenantConfiguration
                    {
                        Key = element.Name,
                        Name = element.Name,
                        DbType = element.DbType,
                        HostValues = element.DomainList,
                        Properties = element.PropertyList,
                        AutoPersist = element.AutoPersist,
                        Connectionstring = element.Connectionstring
                    };

                    var tenant = new Tenant() { Configuration = conf };
                    tenants.Add(element.Name, tenant);
                }
                return tenants;
            }
        }



        /// <summary>
        /// Reads out all informatin about a Tenant before they are created.
        /// Simply an overload of ConfigCollection.
        /// </summary>
        public static List<Tenant> TenantCollection
        {
            get
            {
                var tenants = new List<Tenant>();
                foreach(var tenant in ConfigCollection)
                    tenants.Add((Tenant)tenant.Value);

                return tenants;
            }
        }


        /// <summary>
        /// Varaiable to check for setup.
        /// </summary>
        /// <returns></returns>
        private static bool CreateEnvironment()
        {
            return Configuration.Persist;
        }


        /// <summary>
        /// A set of databases listed from web.config
        /// </summary>
        private static List<DatabaseSection> Databases
        {
            get { return ConvertToList(Collection); }
        }


        /// <summary>
        /// Convert a Collection to a List<T>
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        private static List<DatabaseSection> ConvertToList(ICollection col)
        {
            return Collection.CollectionToList<DatabaseSection>();
        }


        /// <summary>
        /// The DatabaseCollection. 
        /// </summary>
        private static DatabaseCollection Collection
        {
            get { return Configuration.Databases; }
        }


        /// <summary>
        /// Get the SessionFactoryConfiguration
        /// </summary>
        private static SessionFactoryConfiguration Configuration
        {
            get { return Conf.Configuration.DatabaseCollectionReader.conf; }
        }
    }
}
