﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boot.Multitenancy.Extensions
{
    public static partial class BootExtensions
    {
        /// <summary>
        /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        /// Used for testing and example purposes.
        /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        /// Returns a UL LI list as html.
        /// </summary>
        /// <typeparam name="T">Type of Node</typeparam>
        /// <param name="rootItems">A list of Nodes</param>
        /// <param name="childrenProperty">A list of childnodes.</param>
        /// <param name="itemContent">The linktext</param>
        /// <param name="urlContent">The url</param>
        /// <returns>A string ith UL li elements.</returns>
        public static string TreeView<T>(this IEnumerable<T> rootItems, Func<T, IEnumerable<T>> childrenProperty, Func<T, string> itemContent, Func<T, string> urlContent)
        {
            if (rootItems == null || !rootItems.Any()) return null;

            var builder = new StringBuilder();
            builder.AppendLine("<ul>");

            foreach (var item in rootItems)
            {
                builder.AppendLine("  <li>")
                        .Append(string.Format("<a href=\"{0}\">{1}</a>", 
                            urlContent(item), 
                            itemContent(item)))
                        .Append("</li>");

                var childContent = TreeView(childrenProperty(item), childrenProperty, itemContent, urlContent);

                if (childContent != null)
                    builder.Append(childContent);
            }
            builder.Append("</ul>");
            return builder.ToString();
        }
    }
}
