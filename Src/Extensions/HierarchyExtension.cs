﻿using Boot.Multitenancy.Intrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boot.Multitenancy.Extensions
{
    public static partial class BootExtensions
    {
        /// <summary>
        /// Add's a Parent - child relation to to items.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="collection">Collection of Nodes</param>
        /// <returns>A list of RootNodes/returns>
        public static IEnumerable<NodeBase<T>> ToHierarchy<T>(this IEnumerable<NodeBase<T>> collection) where T : Node
        {
            var lookup = new Dictionary<int, NodeBase<T>>();
            var nested = new List<NodeBase<T>>();

            foreach (var item in collection)
            {
                if (lookup.ContainsKey(item.Data.ParentId)) {
                    lookup[item.Data.ParentId].Nodes.ToList().Add(item);
                } else {
                    nested.Add(item);
                }
                lookup.Add(item.Data.Id, item);
            }
            return nested;
        }


        /// <summary>
        /// Add's T to a BaseNode<T> list, typically a inherited Node.
        /// </summary>
        /// <typeparam name="T">Node</typeparam>
        /// <param name="collection">Takes a collection of type Node</param>
        /// <returns>A list of nodes added to BaseNode<T> List</returns>
        public static IEnumerable<NodeBase<T>> ToBaseList<T>(this IEnumerable<T> collection) where T : Node
        {
            var list = new List<NodeBase<T>>();
            foreach (var item in collection)
                list.Add(new NodeBase<T>(item));

            return list.ToHierarchy<T>();
        }


        /// <summary>
        /// Takes an hierarchy and turns into a flat list.
        /// </summary>
        /// <typeparam name="Node">Node</typeparam>
        /// <param name="collection">Takes a collection of type Node</param>
        /// <returns>Collection of type Node</returns>
        public static IEnumerable<NodeBase<T>> ToFlatten<T>(this IEnumerable<NodeBase<T>> collection)
        {
            return collection.Flatten(c => c.Nodes).Reverse();
        }


        private static IEnumerable<NodeBase<T>> Flatten<T>(this IEnumerable<NodeBase<T>> items, Func<NodeBase<T>, IEnumerable<NodeBase<T>>> getChildren)
        {
            var stack = new Stack<NodeBase<T>>();
            foreach (var item in items)
                stack.Push(item);

            while (stack.Count > 0)
            {
                var current = stack.Pop();
                yield return current;

                var children = getChildren(current);
                if (children == null) continue;

                foreach (var child in children)
                    stack.Push(child);
            }
        }
    }
}
